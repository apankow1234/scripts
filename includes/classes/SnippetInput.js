class SnippetInput {
    constructor() {
        this.snippet  = new Snippet();
        this.fieldset = document.createElement( 'fieldset' );
        this.legend   = document.createElement( 'legend' );
        this.input    = document.createElement( 'textarea' );
        this.navLink  = document.createElement( 'a' );
        this.input.tabIndex = "";
        this.fieldset.appendChild(this.legend);
        this.fieldset.appendChild(this.input);
        SnippetInput.AllInputs.push( this );
    }

    setTitle( newTitle ) {
        this.snippet.Beat.Text = this.legend.innerText = newTitle;
        this.fieldset.id  = newTitle.toLowerCase();
        this.navLink.innerText = `${newTitle}`;
        this.navLink.href = `#${this.fieldset.id}`;
        this.input.name   = this.input.id = this.fieldset.id + "Snippet";
    }

    setText( newText ) {
        this.snippet.addText( newText );
        this.input.value = newText;
    }

    FromJSON( snippetJSON ) {
        let keys = Object.keys( snippetJSON );
        this.setTitle( keys[0] );
        this.setText( snippetJSON[ keys[0] ] );
        return this;
    }

    renderInput( container ) {
        container.appendChild( this.fieldset );
        console.log( this.fieldset.parentNode );
        return this;
    }

    renderLink( container ) {
        container.appendChild( this.navLink );
        return this;
    }
}
SnippetInput.AllInputs = [];

class QuestionSnippetInput extends SnippetInput {
    constructor() {
        super();
        this.fieldset.removeChild(this.input);
        this.input             = document.createElement( 'input' );
        this.input.type        = "text";
        this.input.tabIndex    = "";
        this.input.placeholder = "What do you want to ask?";
        this.fieldset.appendChild(this.input);
        this.setTitle( QuestionSnippet.DEFAULT );
    }

    FromJSON( snippetJSON ) {
        let keys = Object.keys( snippetJSON );
        this.setText( snippetJSON[ keys[0] ] );
        return this;
    }
}

class AnswersSnippetInput extends SnippetInput {
    constructor() {
        super();
        this.fieldset.removeChild(this.input);
        this.answersTable  = document.createElement('table');
        this.answersTable.innerHTML = `
        <thead>
            <tr>
                <th>Is Correct?</th>
                <th>Answer</th>
                <th>Remove</th>
            </tr>
        </thead>
        `;
        this.answerInputs = [];
        this.answersTableBody = document.createElement('tbody');
        this.answersTable.appendChild( this.answersTableBody );
        this.setTitle( AnswersOptionsSnippet.DEFAULT );
    }

    FromJSON( snippetJSON ) {
        let keys = Object.keys( snippetJSON );
        // console.log( keys, snippetJSON );
        let answers = Object.keys( snippetJSON[ keys[0] ] );
        answers.forEach( ( ans )=>{
            let n = new AnswerSnippetInput().setAnswer( ans, snippetJSON[ keys[0] ][ ans ] );
            if( n.text.value )
                n.text.placeholder = n.text.value;
            this.answerInputs.push(
                n
            );
        } );
        this.answerInputs.forEach( (aI, idx)=>{
            aI.checkbox.name += idx;
            aI.text.name += idx;
            // console.log( aI );
            let row = document.createElement( 'tr' );
            row.innerHTML = `
            <td>${aI.checkbox.outerHTML}</td>
            <td>${aI.text.outerHTML}</td>
            <td><button name="rem-${aI.text.name}" class="remove-answer" data-for="answer-${idx}">✗</button></td>
            `;
            this.answersTableBody.appendChild( row );
        } );
        this.answersTable.appendChild( this.answersTableBody );
        this.fieldset.appendChild( this.answersTable );
        return this;
    }

    reorderRow( row, newIndex ) {
        
    }

    reorderColumn( row, newIndex ) {
        
    }
}

class AnswerSnippetInput {
    constructor() {
        this.value = "";
        this.isCorrect = false;
        let test = document.createElement( 'form' );
        test.innerHTML = `<input type="text" name="answer" placeholder="The Answer Goes Here" tabindex></input>
        <input type="checkbox" name="isCorrect" tabindex></input>`;
        [ this.text, this.checkbox ] = test.children;
        AnswerSnippetInput.AllInputs.push( this );
    }

    setAnswer( answer, isCorrect = false ) {
        this.value = this.text.value = answer;
        this.isCorrect = this.checkbox.checked = isCorrect;
        return this;
    }
}
AnswerSnippetInput.AllInputs = [];

class SequenceInputForm {
    constructor() {
        SequenceInputForm.AllInputs.push( this );
        this.form = document.createElement('form');
        this.nav  = document.createElement('nav');
        this.div  = document.createElement('div');
        this.btn  = document.createElement('button');
        let btnAttrs  = { "type" : "submit", "id" : `saveBtn${SequenceInputForm.AllInputs.length}` };
        Object.keys(btnAttrs).forEach( k=>this.btn.setAttribute( k, btnAttrs[k] ) );
        this.btn.innerText = `Save`;
        this.snippets      = [];
    }

    FromJSON( sequenceJSON ) {
        let q1Keys = Object.keys( sequenceJSON );
        for(let i=0; i<q1Keys.length; i++){
            let t = {};
            let newSnippet  = null;
            let keyIsString = q1Keys[i].constructor.name == "String";
            if( keyIsString ) {
                let valIsString = sequenceJSON[ q1Keys[i] ].constructor.name == "String";
                t[ q1Keys[i] ] = sequenceJSON[ q1Keys[i] ];
                if( valIsString ) {
                    if( q1Keys[i].toLowerCase() == QuestionSnippet.DEFAULT.toLowerCase() ) {
                        newSnippet = new QuestionSnippetInput().FromJSON( t );
                    } else {
                        newSnippet = new SnippetInput().FromJSON( t );
                    }
                } else {
                    if( q1Keys[i].toLowerCase() == AnswersOptionsSnippet.DEFAULT.toLowerCase() ) {
                        newSnippet = new AnswersSnippetInput().FromJSON( t );
                    }
                }
            }
            if( newSnippet != null ) {
                this.snippets.push( newSnippet );
            }
        }
        return this;
    }

    render( container ) {
        container.appendChild( this.form );
        this.div.classList.add( 'fields' );
        let navlist = document.createElement('ul');
        this.nav.appendChild( navlist );
        this.form.appendChild( this.nav );
        this.form.appendChild( this.div );
        this.snippets.forEach( (s)=>{
            let navItem = document.createElement('li');
            navlist.appendChild( navItem );
            this.div.appendChild( s.fieldset );
            navItem.appendChild( s.navLink );
            if( s instanceof AnswersSnippetInput ) {
                s.answerInputs.forEach((ans)=>{
                    ans.checkbox.checked = ans.isCorrect;
                    ans.text.value = ans.value;
                });
            }
        } );
        this.form.appendChild( this.btn );
        container.appendChild( this.form );
        return this;
    }
}
SequenceInputForm.AllInputs = [];

class QuestionSequenceInputForm extends SequenceInputForm {
    constructor() {
        super();
    }
}
class OrderedText {
    constructor() {
        this.Order = 0;
        this.CurrentState = [];
        this.Parent = null;
    }

    get text() {
        return this.getText();
    }

    get accountsFor() {
        if( this.Parent != null ) {
            return this.text.length / this.Parent.text.length;
        } else {
            return 1;
        }
    }

    toData() {
        return {
            "order" : this.Order,
            "child" : this.CurrentState.map( (el)=>{
                            if( el.constructor.name == "String" ) {
                                return el;
                            } else if( el instanceof OrderedText ) {
                                return el.toData();
                            } else if( el instanceof Word ) {
                                return el.toData();
                            }
                        } )
        };
        
    }

    getText( speaker ) {
        return this.CurrentState.map( (el)=>{
            if( el.constructor.name == "String" ) {
                return `\n${el}`;
            } else if( el instanceof OrderedText ) {
                return el.getText( speaker );
            } else if( el instanceof Word ) {
                return el.getText( speaker );
            }
        } ).join(' ');
    }

    addText( text ) {
        this.CurrentState.push( text );
        if( text instanceof OrderedText ) {
            text.Parent = this;
        }
        return this;
    }

    removeText( text ) {
        this.CurrentState.push( text );
        if( text instanceof OrderedText ) {
            text.Parent = this;
        }
        return this;
    }
}

class Word {
    constructor( word ) {
        let existing = null;
        this.Instances = [];
        Word.AllWords.forEach( (w)=>{
            if( w.lowerCase == word.toLowerCase() ) {
                existing = w;
            }
        } );
        if( existing ){
            return new WordInstance( existing, word );
        } else {
            this.Text = word;
            this.Pronunciations = {};
            Word.AllWords.push( this );
            return new WordInstance( this, word );
        }
    }

    static getCase( word ) {
        /**
         * 0 : Titlecase
         * 1 : UPPERCASE
         * 2 : lowercase
         * 3 : MIXedcaSe
         */
        let lowers = 0;
        let caps = 0;
        let total = word.length;
        for( let i=0; i < total; i++ ) {
            if( word[i] == word[i].toLowerCase() ) {
                lowers++;
            } else if( word[i] == word[i].toUpperCase() ) {
                caps++;
            }
        }
        if(caps == total) {
            return 1;
        } else if(lowers == total) {
            return 2;
        } else if(word[0] == word[0].toUpperCase() && caps == 1) {
            return 0;
        } else {
            return 3;
        }
    }

    static getWord( spelling ) {
        Word.AllWords.filter(
            w => (w.lowerCase == spelling.toLowerCase()) 
        );
    }

    get titleCase() {
        return this.Text[0].toUpperCase() + this.Text.substr(1).toLowerCase();
    }

    get upperCase() {
        return this.Text.toUpperCase();
    }

    get lowerCase() {
        return this.Text.toLowerCase();
    }

    setPronunciation( speaker, ipa ) {
        this.Pronunciations[ speaker ] = ipa;
        return this;
    }

    getText( speaker ) {
        let text = this.Text;
        if( speaker && this.Pronunciations[ speaker ] ) {
            text += ` [\/\`${this.Pronunciations[ speaker ]}\`\/]`;
        }
        return text;
    }

    toData() {
        let text = this.Text;
        let keys = Object.keys( this.Pronunciations );
        keys.forEach( (k)=>{
            text += ` [${k}:  \/\`${this.Pronunciations[k]}\`\/]`;
        } );
        return text;
    }
}
Word.AllWords = [];

class WordInstance {
    constructor( word, instanceCasing ) {
        this.Instance = instanceCasing;
        this.Word = word;
        this.Casing = Word.getCase( this.Instance );
        word.Instances.push( this );
        // .Instances.push( this );
        WordInstance.AllWordInstances.push( this );
    }

    isSpelledCorrectly( wordsListDictionary ) {
        let matches = wordsListDictionary.filter( word => word.toLowerCase() == this.Word.lowerCase );
        return ( matches.length > 0 );
    }
    
    get text() {
        return this.render();
    }

    render() {
        switch( this.Casing ) {
            case 0  : return this.Word.titleCase; // Titlecase
            case 1  : return this.Word.upperCase; // UPPERCASE
            case 2  : return this.Word.lowerCase; // lowercase
            default : return this.Instance;       // MIXedcaSe
        }
    }
}
WordInstance.AllWordInstances = [];

class Beat extends OrderedText {
    constructor( text ) {
        super();
        this.Text = text || "";
        this.Snippet = null;
        Beat.AllBeats.push( this );
    }
}
Beat.AllBeats = [];

class AnswerOption extends OrderedText {
    constructor( beat ) {
        super();
        this.isCorrect = false;
        AnswerOption.AllAnswers.push( this );
    }

    toggleCorrect( toggleTo ) {
        if( toggleTo != undefined && toggleTo != null ) {
            this.isCorrect = ( toggleTo ) ? true : false;
        } else {
            this.isCorrect = ( this.isCorrect ) ? false : true;
        }
        // console.log( `Now: ${this.isCorrect}` )
        return this;
    }
}
AnswerOption.AllAnswers = [];

class Snippet extends OrderedText {
    constructor( beat ) {
        super();
        if( !beat ) {
            this.Beat = new Beat();
        } else if( beat.constructor.name == "String" ) {
            this.Beat = new Beat( beat );
        } else if( beat.constructor.name == "Beat" ) {
            this.Beat = beat;
        }
        
        this.Beat.Snippet = this;
        Snippet.AllSnippets.push( this );
    }

    get title() {
        return this.Beat.Text;
    }
}
Snippet.AllSnippets = [];

class QuestionSnippet extends Snippet {
    constructor( beat ) {
        super( beat || QuestionSnippet.DEFAULT );
        this.answerOptions = null;
        QuestionSnippet.AllSnippets.push( this );
    }

    addAnswerOptions( answersOptionsSnippet ) {
        this.answerOptions = answersOptionsSnippet;
        this.answerOptions.Question = this;
    }
}
QuestionSnippet.DEFAULT = "Question";
QuestionSnippet.AllSnippets = [];

class AnswersOptionsSnippet extends Snippet {
    constructor( beat ) {
        super( beat || AnswersOptionsSnippet.DEFAULT );
        this.Question = null;
        this.answers = [];
        this.defaultCount = 3;
        this.Reveal = null;
        AnswersOptionsSnippet.AllSnippets.push( this );
    }

    addAnswer( answerOption ) {
        this.answers.push( answerOption );
        this.CurrentState.push( answerOption );
    }

    getText( speaker ) {
        return this.CurrentState.map( (el)=>{
            if( el.constructor.name == "AnswerOption" ) {
                return `${el.text}`;
            } else if( el.constructor.name == "String" ) {
                return `\n${el}`;
            } else if( el instanceof OrderedText ) {
                return el.getText( speaker );
            } else if( el instanceof Word ) {
                return el.getText( speaker );
            }
        } ).join(' ');
    }
}
AnswersOptionsSnippet.DEFAULT = "Answers";
AnswersOptionsSnippet.AllSnippets = [];

class RevealSnippet extends Snippet {
    constructor( beat ) {
        super( beat || RevealSnippet.DEFAULT  );
        this.DefaultText = "The correct answer is... ";
        this.Answers = null;
        RevealSnippet.AllSnippets.push( this );
    }

    getText( speaker ) {
        return "\n"+this.DefaultText + this.Answers.answers.filter( (ans)=>{
            return ans.isCorrect;
        } ).map( (ans)=>{
            return ans.text;
        } ).join(' AND ');
    }

    setAnswers( answersOptionsSnippet = null ) {
        this.Answers = answersOptionsSnippet;
        this.Answers.Reveal = this;
        return this;
    }
}
RevealSnippet.DEFAULT = "Answer Reveal";
RevealSnippet.AllSnippets = [];

class Sequence extends OrderedText {
    constructor() {
        super();
        this.Snippets = {};
        Sequence.AllSequences.push( this );
    }

    addSnippet( snippet ) {
        this.Snippets[ snippet.Beat.Text ] = snippet;
        this.CurrentState.push( snippet );
    }
}
Sequence.AllSequences = [];

class QuestionSequence extends Sequence {
    constructor() {
        super();
        this.clueSnippet    = new Snippet( "Clue" );
        this.quesionSnippet = new QuestionSnippet();
        this.answersSnippet = new  AnswersOptionsSnippet();
        this.quesionSnippet.addAnswerOptions( this.answersSnippet );
        for( let i=0; i < 3; i++ ) {
            this.answersSnippet.addAnswer( new AnswerOption() );
        }
        this.qBlockSnippet  = new Snippet( "Q-Block" );
        this.revealSnippet  = new RevealSnippet().setAnswers(this.answersSnippet);
        this.aBlockSnippet  = new Snippet( "A-Block" );
        [
            this.clueSnippet,
            this.quesionSnippet,
            this.answersSnippet,
            this.qBlockSnippet,
            this.revealSnippet,
            this.aBlockSnippet
        ].forEach( (s, idx)=>{
            s.Order = idx+1;
            this.addSnippet( s );
        } );
        QuestionSequence.AllSequences.push( this );
    }

    FromJSON( sequenceJSON ) {
        let snippets = Object.keys( sequenceJSON );
        for( let i=0; i < snippets.length; i++ ) {
            if( snippets[i] == AnswersOptionsSnippet.DEFAULT ) {
                let answers = Object.keys( sequenceJSON[ snippets[i] ] );
                answers.forEach( (ans, idx)=>{
                    if( idx < 3 ) {
                        this.Snippets[ AnswersOptionsSnippet.DEFAULT ].answers[ idx ].addText( ans ).toggleCorrect( sequenceJSON[ snippets[i] ][ ans ] );
                    } else {
                        this.Snippets[ AnswersOptionsSnippet.DEFAULT ].addAnswer( new AnswerOption().addText( ans ).toggleCorrect( sequenceJSON[ snippets[i] ][ ans ] ) );
                    }
                } );
            } else {
                this.Snippets[ snippets[i] ].addText( sequenceJSON[ snippets[i] ] );
            }
        }
    }
}
QuestionSequence.AllSequences = [];
